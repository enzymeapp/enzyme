package com.enzymeapp.enzyme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FriendChatActivity extends AppCompatActivity {

    //Class Init
    RecyclerView listView;
    ArrayList<UserMessage> items;
    ArrayList<UserMessage> newItems;
    UserMessageAdapter adapter;
    EditText editText;
    Button button;
    ChatManager chatManager;
    EntityBareJid jid;
    TextView friendHeader;
    File daMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get USERID From Lobby Page and init friend chat layout

        Intent friendIntent = getIntent();
        String userID = friendIntent.getStringExtra(MainActivity.EXTRA_USERID);
        String userNick = friendIntent.getStringExtra(MainActivity.EXTRA_USERNICKNAME);


        //Init and assignment
        setContentView(R.layout.activity_friend_chat);
        friendHeader = findViewById(R.id.friendheadertext);
        listView = findViewById(R.id.recylerthing);

        daMessages = new File(this.getFilesDir(), userNick + "Messages.txt");
        newItems = new ArrayList<>();
        items = new ArrayList<>();
        ArrayList<UserMessage> temp = loadFromDevice();
        if (temp != null)
            items = temp;

        editText = findViewById(R.id.editTextTextPersonName);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(layoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        adapter = new UserMessageAdapter(this, items);
        button = (Button) findViewById(R.id.button);
        listView.setAdapter(adapter);



        friendHeader.setText(userNick);


        //JID Domain generator **SMACK**

        DomainBareJid serviceName = null;

        {
            try {
                serviceName = JidCreate.domainBareFrom("enzymeapp.com");
            } catch (XmppStringprepException xmppStringprepException) {
                xmppStringprepException.printStackTrace();
            }
        }

        DomainBareJid finalServiceName = serviceName;


        //XMPPTCP Config generator **SMACK**

        XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                .setUsernameAndPassword("test", "test")
                .setHost("msg.enzymeapp.com")
                .setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible)
                .setXmppDomain(finalServiceName)
                .setPort(5222)
                .build();


        //XMPPTCP Connection Establish **SMACK**
        //We want to change this to just a XMPP connection ASAP

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(new Runnable() {
            @Override
            public void run() {

                //Background work here
                AbstractXMPPConnection conn1 = new XMPPTCPConnection(config);
                try {
                    conn1.connect();
                    if (conn1.isConnected()) {
                        Log.w("app", "conn done");
                    }
                    conn1.login();

                    if (conn1.isAuthenticated()) {
                        Log.w("app", "Auth done");

                    }
                } catch (Exception e) {
                    Log.w("app", e.toString());
                }


                //Friend-Level Chat Manager init
                //SERVICE WORKS NEED TO BIND TO THIS ACTIVITY AND GET ABSTRACTXMPPCONNECTION

                chatManager = ChatManager.getInstanceFor(conn1);


                chatManager.addIncomingListener(new IncomingChatMessageListener() {
                    @Override
                    public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                addMessage(message.getBody(), userNick, false);

                            }
                        });
                    }
                });

                // Friend JID init

                EntityBareJid jid = null;
                try {
                    jid = JidCreate.entityBareFrom(userID + "@enzymeapp.com");
                } catch (XmppStringprepException e) {
                    e.printStackTrace();
                }

                EntityBareJid finalJid = jid;


                // Incoming Listener Init and new UI Thread


                //Send message button

                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        String text = editText.getText().toString();

                        try {
                            sendMessage(text, finalJid);
                        } catch (SmackException.NotConnectedException | InterruptedException e) {
                            e.printStackTrace();
                        }
                        addMessage(text, "User 1", true);
                        editText.setText("");
                    }
                });


                editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        boolean handled = false;
                        if (i == EditorInfo.IME_ACTION_SEND) {
                            button.callOnClick();
                            handled = true;
                        }
                        return handled;
                    }
                });
            }
        });


    }


    /**
     * Method used to save the list of items to the device. It converts the UserMessages to strings, then add those to strings to the file.
     */
    public void saveToDevice() {

        FileWriter fw = null; //create a new FileWriter object to allow us to write to the created file
        try {
            fw = new FileWriter(daMessages.getAbsoluteFile(), true);
        } catch (IOException e) {

        }
        BufferedWriter bw = new BufferedWriter(fw); //create a new BufferedWriter object to write to the file
        ArrayList<String> daMessages = new ArrayList<>();
        for (UserMessage message : newItems) {

            daMessages.add(message.toString());
        }
        for (String message : daMessages) {
            try {

                bw.write(message + "\n"); //Write to the file
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try { //Try to close the file.
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to load the messages from the device.
     * @return an array list of type UserMessage, that will then populate the chat
     */
    public ArrayList<UserMessage> loadFromDevice() {


        ArrayList<String> messages = new ArrayList<>();
        try {
            Scanner myReader = new Scanner(daMessages);
            while (myReader.hasNextLine()) {
                messages.add(myReader.nextLine());
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        for (String message : messages) {
            String[] indivMessage = message.split(" ");
            Boolean friend = null;
            if (indivMessage[2].equals("true"))
                friend = true;
            else
                friend = false;
            String newMessage = indivMessage[1];
            for (int i = 2; i <= indivMessage.length - 2; i++){
                newMessage = newMessage + " " + indivMessage[i];
            }
            UserMessage addMessage = new UserMessage(indivMessage[0], newMessage, friend);

            items.add(addMessage);
        }
        return items;
    }


    //FUNCTIONS

    public void sendMessage(String newMessage, EntityBareJid to) throws SmackException.NotConnectedException, InterruptedException {

        if (chatManager != null) {
            Chat sendingChat = chatManager.chatWith(to);
            sendingChat.send(newMessage);
        }
    }


    public void addMessage(String message, String userID, Boolean friendly) {
        UserMessage userMessage = new UserMessage(userID, message, friendly);
        //items.add(message);
        newItems.add(userMessage);
        adapter.addMessage(userMessage);
        adapter.notifyItemInserted(adapter.getItemCount() -1);


        Log.d("message", "Sent");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveToDevice();
    }
}
