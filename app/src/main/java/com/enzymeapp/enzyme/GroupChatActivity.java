package com.enzymeapp.enzyme;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.UserHandle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GroupChatActivity extends AppCompatActivity {

    //Init
    RecyclerView listViewGroup;
    ArrayList<UserMessage> itemsGroup;
    ArrayList<UserMessage> newItemsGroup;
    GroupMessageAdapter adapterGroup;
    TextView groupTitleText;
    EditText editTextGroup;
    Button buttonGroup;
    MultiUserChatManager mucChatManager;
    MultiUserChat muC;
    File daMessages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);

        //Get intent data
        Intent groupIntent = getIntent();
        String groupName = groupIntent.getStringExtra(MainActivity.EXTRA_GROUPID);


        //INIT
        listViewGroup = findViewById(R.id.group_activity_recycler);
        buttonGroup = findViewById(R.id.buttongroupChat);
        editTextGroup = findViewById(R.id.editTextGroup);
        groupTitleText = findViewById(R.id.groupchat_headertext);

        daMessages = new File(this.getFilesDir(), "Messages.txt");
        itemsGroup = new ArrayList<>();
        newItemsGroup = new ArrayList<>();
        ArrayList<UserMessage> temp = loadFromDevice();
        if (temp != null)
            itemsGroup = temp;


        //Init continued
        RecyclerView.LayoutManager layoutManagerGroup = new LinearLayoutManager(this);
        listViewGroup.setLayoutManager(layoutManagerGroup);
        adapterGroup = new GroupMessageAdapter(this, itemsGroup);
        listViewGroup.setAdapter(adapterGroup);
        groupTitleText.setText(groupName);

        //JID Domain generator **SMACK**

        DomainBareJid serviceName = null;

        {
            try {
                serviceName = JidCreate.domainBareFrom("enzymeapp.com");
            } catch (XmppStringprepException xmppStringprepException) {
                xmppStringprepException.printStackTrace();
            }
        }

        DomainBareJid finalServiceName = serviceName;



        //XMPPTCP Config generator **SMACK** DOES NOT WORK

        XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                .setUsernameAndPassword("test", "test")
                .setHost("groups.enzymeapp.com")
                .setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible)
                .setXmppDomain(finalServiceName)
                .setPort(5222)
                .build();



        //XMPPTCP Connection Establish **SMACK**
        //We want to change this to just a XMPP connection ASAP

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(new Runnable() {
            @Override
            public void run() {

                AbstractXMPPConnection conn1 = new XMPPTCPConnection(config);
                try {
                    conn1.connect();
                    if (conn1.isConnected()) {
                        Log.w("app", "conn done");
                    }
                    conn1.login();

                    if (conn1.isAuthenticated()) {
                        Log.w("app", "Auth done");

                    }
                } catch (Exception e) {
                    Log.w("app", e.toString());
                }


                // Friend JID init

                EntityBareJid jidGroup = null;
                try {
                    jidGroup = JidCreate.entityBareFrom("tardsonly@groups.enzymeapp.com");
                } catch (XmppStringprepException e) {
                    e.printStackTrace();
                }

                EntityBareJid finalGroupJid = jidGroup;


                //MUC ChatManager Init


                mucChatManager = MultiUserChatManager.getInstanceFor(conn1);


                //Group Chat init

                muC = mucChatManager.getMultiUserChat(finalGroupJid);
                Roster roster = Roster.getInstanceFor(conn1);


                if (muC != null) {
                    muC.addMessageListener(new MessageListener() {
                        @Override
                        public void processMessage(Message message) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    addGroupMessage(message.getBody(), message.getFrom().toString(), false);
                                }
                            });
                        }
                    });
                }
            }});





        //Send button listener
        buttonGroup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String text = editTextGroup.getText().toString();

                addGroupMessage(text, "User 1", true);
                editTextGroup.setText("");
            }
        });

    }

    /**
     * Method used to save the list of items to the device. It converts the UserMessages to strings, then add those to strings to the file.
     */
    public void saveToDevice() {

        FileWriter fw = null; //create a new FileWriter object to allow us to write to the created file
        try {
            fw = new FileWriter(daMessages.getAbsoluteFile(), true);
        } catch (IOException e) {

        }
        BufferedWriter bw = new BufferedWriter(fw); //create a new BufferedWriter object to write to the file
        ArrayList<String> daMessages = new ArrayList<>();
        for (UserMessage message : newItemsGroup) {

            daMessages.add(message.toString());
        }
        for (String message : daMessages) {
            try {

                bw.write(message + "\n"); //Write to the file
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try { //Try to close the file.
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to load the messages from the device.
     * @return an array list of type UserMessage, that will then populate the chat
     */
    public ArrayList<UserMessage> loadFromDevice() {


        ArrayList<String> messages = new ArrayList<>();
        try {
            Scanner myReader = new Scanner(daMessages);
            while (myReader.hasNextLine()) {
                messages.add(myReader.nextLine());
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        for (String message : messages) {
            String[] indivMessage = message.split(" ");
            Boolean friend = null;
            if (indivMessage[2].equals("true"))
                friend = true;
            else
                friend = false;
            String newMessage = indivMessage[1];
            for (int i = 2; i <= indivMessage.length - 2; i++){
                newMessage = newMessage + " " + indivMessage[i];
            }
            UserMessage addMessage = new UserMessage(indivMessage[0], newMessage, friend);

            itemsGroup.add(addMessage);
        }
        return itemsGroup;
    }

    //FUNCTIONS
    public void addGroupMessage(String message, String userID, Boolean friendly) {
        UserMessage userMessage = new UserMessage(userID, message, friendly);
        //items.add(message);
        newItemsGroup.add(userMessage);
        adapterGroup.addMessage(userMessage);
        adapterGroup.notifyItemInserted(adapterGroup.getItemCount() -1);


        Log.d(" group message", "Sent");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveToDevice();
    }
}