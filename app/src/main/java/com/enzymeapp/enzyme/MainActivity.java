package com.enzymeapp.enzyme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MainActivity extends AppCompatActivity {

    //Init for intent data transfers
    public static final String EXTRA_USERID = "com.enzymeapp.enzyme.USERID";
    public static final String EXTRA_USERNICKNAME = "com.enzymeapp.enzyme.USERNICKNAME";
    public static final String EXTRA_GROUPID = "com.enzymeapp.enzyme.GROUPID";

    //Init for main activity classes
    RecyclerView friendRecycler;
    Button addfriendButton;
    Button togroupChatButton;
    BottomSheetBehavior bottomSheetBehavior;
    ArrayList<Friend> friendList;
    FriendListAdapter friendListAdapter;

    //FOR TESTING
    Button loginTester;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Further init with assignments
        setContentView(R.layout.activity_main);
        addfriendButton = findViewById(R.id.addfriend_button);
        togroupChatButton = findViewById(R.id.wip_group_chat_butt);
        friendList = new ArrayList<>();


        //FOR TESTING PURPOSES Friends List Hardcoding
        Friend declan = new Friend("declan", "Declan");
        Friend august = new Friend("august", "August");
        Friend jack = new Friend("jack", "Jack");
        Friend glade = new Friend("jon", "Glade");
        Friend foster = new Friend("foster", "Foster");
        Friend ben = new Friend("peter", "Ben");
        Friend root = new Friend("root", "Root");
        friendList.add(declan);
        friendList.add(august);
        friendList.add(jack);
        friendList.add(glade);
        friendList.add(foster);
        friendList.add(ben);
        friendList.add(root);

        //Init continued
        friendRecycler = findViewById(R.id.friend_list_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        friendRecycler.setLayoutManager(layoutManager);
        friendListAdapter = new FriendListAdapter(friendList);
        friendRecycler.setAdapter(friendListAdapter);
        View friendBottomSheet = findViewById(R.id.friendBottomSheetLayout);
        bottomSheetBehavior = BottomSheetBehavior.from(friendBottomSheet);

        //FOR TESTING
        loginTester = findViewById(R.id.wip_login_test_button);
        //Testing login button switcher listener
        loginTester.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginSwitcher();
            }
        });



        //Listener for friends list name buttons
        friendListAdapter.setItemClickListener(new FriendListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                friendSwitcher(friendList.get(position).getUserXMPP(), friendList.get(position).getUserIDtext());
            }
        });

        //Listener for group chat card buttons
        togroupChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                groupSwitcher("TestGroup");
            }
        });



    }
    /*
    class AuthenticationPagerAdapter extends FragmentStateAdapter {
        private ArrayList<Fragment> fragmentList = new ArrayList<>();

        public AuthenticationPagerAdapter(@NonNull Fragment fragment) {
            super(fragment);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            fragmentList.add(fragment);
        }

        @Override
        public int getItemCount() {
            return 0;
        }
    }
    */

    //Intent inits for groupChat and friendChat activities
    public void groupSwitcher(String groupID) {
        Intent groupChatIntent = new Intent(this, GroupChatActivity.class);
        groupChatIntent.putExtra(EXTRA_GROUPID, groupID);
        startActivity(groupChatIntent);
    }

    public void friendSwitcher(String userID, String userNick) {
        Intent friendChatIntent = new Intent(this, FriendChatActivity.class);
        friendChatIntent.putExtra(EXTRA_USERID, userID);
        friendChatIntent.putExtra(EXTRA_USERNICKNAME, userNick);
        startActivity(friendChatIntent);
    }
    public void loginSwitcher() {
        Intent toLogin = new Intent(this, LoginAuthActivity.class);
        startActivity(toLogin);
    }
}