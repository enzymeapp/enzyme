package com.enzymeapp.enzyme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class LoginAuthActivity extends AppCompatActivity {

    //View inits
    EditText loginUserInput;
    EditText loginPassInput;
    Button loginButton;
    Button toRegisterButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_auth);

        //View find inits
        loginUserInput = findViewById(R.id.username_login);
        loginPassInput = findViewById(R.id.password_login);
        loginButton = findViewById(R.id.btn_login);
        toRegisterButton = findViewById(R.id.register_button);


        //To register listener
        toRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toRegisterSwitcher();
            }
        });

        //Account manager init
        //IF WE USE THIS METHOD WE WILL NEED TO MENTION IT IN PRIVACY POLICY
        AccountManager am = AccountManager.get(this); // "this" references the current Context
        Account[] accounts = am.getAccountsByType("com.enzymeapp");

        if (accounts.length == 1) {
        Account myAccount_ = accounts[0];

            Bundle options = new Bundle();

            am.getAuthToken(
                    myAccount_,                     // Account retrieved using getAccountsByType()
                    "Manage your tasks",            // Auth scope
                    options,                        // Authenticator-specific options
                    this,                           // Your activity
                    new OnTokenAcquired(),          // Callback called when a token is successfully acquired
                    new Handler( new OnError()));    // Callback called if an error occurs
        }
        //This elif condition is almost certainly not the right way to do this.
        //Most of this will have to go.
        else if (accounts.length == 0) {
            String username = loginUserInput.getText().toString();
            String password = loginPassInput.getText().toString();

            //am.addAccount("com.enzymeapp",); //TODO: Figure out how to make a new account type for enzymeapp.com
        }

        URL url = null;
        try {
            url = new URL("https://www.enzymeapp.com/");  //Need to update this. I believe this all assumes an HTTPS connection.
        } catch (MalformedURLException e) {e.printStackTrace();}

        URLConnection conn = null;
        try {
            assert url != null;
            conn = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {e.printStackTrace();}

        assert conn != null;
        conn.addRequestProperty("client_id", "CLIENT_ID"); //Not sure was is needed here.
        conn.addRequestProperty("client_secret", "CLIENT_SECRET");//Not sure was is needed here.
        conn.setRequestProperty("Authorization", "OAuth " + AccountManager.KEY_AUTHTOKEN);


    }


    private class OnTokenAcquired implements AccountManagerCallback<Bundle> {
        @Override
        public void run(AccountManagerFuture<Bundle> result) {
            // Get the result of the operation from the AccountManagerFuture.
            Bundle bundle = null;
            try {
                bundle = result.getResult();
            } catch (AuthenticatorException | IOException | OperationCanceledException e) {
                e.printStackTrace();
            }

            // The token is a named value in the bundle. The name of the value
            // is stored in the constant AccountManager.KEY_AUTHTOKEN.
            assert bundle != null;
            String token = bundle.getString(AccountManager.KEY_AUTHTOKEN);

            Intent launch = null;
            try {
                launch = (Intent) result.getResult().get(AccountManager.KEY_INTENT);
            } catch (AuthenticatorException | IOException | OperationCanceledException e) {
                e.printStackTrace();
            }
            if (launch != null) {
                //registerForActivityResult(launch, );
        }
    }
}

    private class OnError implements Handler.Callback {
        @Override
        public boolean handleMessage(@NonNull Message message) {
            return false;
        }
    }

    public void toRegisterSwitcher() {
        Intent toRegister = new Intent(this, RegisterActivity.class);
        startActivity(toRegister);
    }


    }