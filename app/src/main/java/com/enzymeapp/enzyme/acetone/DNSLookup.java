package com.enzymeapp.enzyme.acetone;

import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.SRVRecord;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

public class DNSLookup {

    public static void main(String[] args) {
        String query = "_xmpp-client._tcp.enzymeapp.com";
        try {
            Record[] records = new Lookup(query, Type.SRV).run();
            if (records != null) {
                for (Record record : records) {
                    SRVRecord srv = (SRVRecord) record;
                    String hostname = srv.getTarget().toString().replaceFirst("\\.$", "");
                    int port = srv.getPort();
                    System.out.println(hostname + ":" + port);
                }
            } else {
                System.out.println("No Records Found");
            }
        } catch (TextParseException e) {
            e.printStackTrace();
        }
    }
}