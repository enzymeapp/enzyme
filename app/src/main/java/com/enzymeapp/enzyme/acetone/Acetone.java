package com.enzymeapp.enzyme.acetone;

import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Acetone {
    public static void main(String[] args) {
        Acetone client = new Acetone();
        //client.TLSHandshake();
    }
    private void TLSHandshake(String hostname, int port) {
        try {
            //Set connection parameters.
            InetAddress host = InetAddress.getByName(hostname);
            System.out.println("Attempting connection to " + host + " on port " + port);

            //Initiate TCP socket connection.
            Socket socket = new Socket(host, port);
            System.out.println("Successful connection to " + socket.getRemoteSocketAddress());

            //Initialize data channels.
            PrintWriter toServer = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            //Send request to the server.
            toServer.println("<?xml version='1.0' encoding='UTF-8' standalone='no'?>");
            toServer.println("<stream:stream xmlns:stream='http://etherx.jabber.org/streams' " +
                    "xmlns='jabber.client' from='idiot@xmpp.shangrabruh.xyz' " +
                    "to='enzymeapp.com' version='1.0' xml:lang='en'>");
            toServer.println("<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>");
            toServer.println("<stream:stream xmlns:stream='http://etherx.jabber.org/streams' " +
                    "xmlns='jabber.client' from='idiot@xmpp.shangrabruh.xyz' " +
                    "to='enzymeapp.com' version='1.0' xml:lang='en'>");
            toServer.println("<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='DIGEST-MD5'></auth>");

            //Receive response from server.
            String inputLine;
            if ((inputLine = fromServer.readLine()) != null) {
                System.out.println("Server responded: " + inputLine);
                String id = XMLFactory.getNodeAttribute(inputLine, "stream:stream", "id");
                String from = XMLFactory.getNodeAttribute(inputLine, "stream:stream", "from");
                System.out.println("The stream id from " + from + " is: " + id);
            }

            //Close data channels and socket.
            toServer.close();
            fromServer.close();
            socket.close();
        } catch (SAXException | ParserConfigurationException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
