package com.enzymeapp.enzyme.acetone;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

public class XMLFactory {
    public static String getNodeAttribute(String xmlString, String tagString, String attrString) throws ParserConfigurationException, IOException, SAXException {
        //Builds XML string as a Document object.
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xmlString)));

        //Uses Document methods to find the specified attribute of a node.
        NodeList nodes = doc.getElementsByTagName(tagString);
        Element node = (Element) nodes.item(0);
        return node.getAttributes().getNamedItem(attrString).getNodeValue();
    }
}
