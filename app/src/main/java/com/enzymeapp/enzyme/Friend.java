package com.enzymeapp.enzyme;

public class Friend {
    private String userXMPP;
    private String userIDtext;

    public Friend(String userXMPP, String userIDtext) {
        this.userXMPP = userXMPP;
        this.userIDtext = userIDtext;
    }

    public String getUserXMPP() {
        return userXMPP;
    }

    public String getUserIDtext() {
        return userIDtext;
    }

    public void setUserID(String userID) {
        this.userXMPP = userXMPP;
    }

    public void setMessage(String message) {
        this.userIDtext = userIDtext;
    }
}
