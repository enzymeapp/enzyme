package com.enzymeapp.enzyme;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.MyFriendHolder> {

    private ArrayList<Friend> friendList;
    private OnItemClickListener itemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setItemClickListener(OnItemClickListener listener) {
        itemClickListener = listener;
    }

    public FriendListAdapter(ArrayList<Friend> friendList) {
        this.friendList = friendList;
    }

    public static class MyFriendHolder extends RecyclerView.ViewHolder {
        private TextView userIDtext;

        public MyFriendHolder(final View view, OnItemClickListener listener) {
            super(view);
            userIDtext = view.findViewById(R.id.friend_list_friend_id_text);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        //changed from getAbsoluteAdapterPosition to getAdapterPosition
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }


            });
        }
    }

    @NonNull
    @Override
    public FriendListAdapter.MyFriendHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.friends_list_row, parent, false);
        return new MyFriendHolder(itemView, itemClickListener);
    }


    @Override
    public void onBindViewHolder(@NonNull FriendListAdapter.MyFriendHolder holder, int position) {
        String userIDtext = friendList.get(position).getUserIDtext();

        holder.userIDtext.setText(userIDtext);

    }

    @Override
    public int getItemCount() {
        return friendList.size();
    }
}
