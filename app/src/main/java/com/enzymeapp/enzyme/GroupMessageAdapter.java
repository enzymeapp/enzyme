package com.enzymeapp.enzyme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class GroupMessageAdapter extends RecyclerView.Adapter<GroupMessageAdapter.MyViewHolder> {

    private ArrayList<UserMessage> usermessageList;
    private Context context;


    public GroupMessageAdapter(Context context, ArrayList<UserMessage> itemsGroup) {
        this.usermessageList = itemsGroup;
        this.context = context;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView userText;
        private TextView messageText;

        public MyViewHolder(final View view) {
            super(view);
            userText = view.findViewById(R.id.message_view_user_id_text);
            messageText = view.findViewById(R.id.message_view_message_text);
        }
    }

    @NonNull
    @Override
    public GroupMessageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_message_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupMessageAdapter.MyViewHolder holder, int position) {
        String user = usermessageList.get(position).getUserID();
        String message = usermessageList.get(position).getMessage();

        holder.userText.setText(user);
        holder.messageText.setText(message);
    }

    @Override
    public int getItemCount() {
        return usermessageList.size();
    }

    public void addMessage(UserMessage message) {
        usermessageList.add(message);
    }
}