package com.enzymeapp.enzyme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity {
    EditText regiUserInput;
    EditText regiPassInput;
    Button regiButton;
    Button toLoginButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        regiUserInput = findViewById(R.id.username_register);
        regiPassInput = findViewById(R.id.password_register);
        regiButton = findViewById(R.id.btn_register);
        toLoginButton = findViewById(R.id.back_login_button);

        toLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toLoginSwitcher();
            }
        });



    }

    public void toLoginSwitcher() {
        Intent backtoLogin = new Intent(this, LoginAuthActivity.class);
        startActivity(backtoLogin);
    }

}