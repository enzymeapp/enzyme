package com.enzymeapp.enzyme;

public class UserMessage {
    private String userID;
    private String message;
    private Boolean friendly;

    public UserMessage(String userID, String message, Boolean friendly) {
        String[] id = userID.split(" ");
        String newID = "";
        for (String name : id)
            newID = newID+name;
        this.userID = newID;
        this.message = message;
        this.friendly = friendly;
    }

    public String getUserID() {
        return userID;
    }

    public String getMessage() {
        return message;
    }

    public Boolean isFriendly() {return friendly; }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setFriendly(Boolean friendly) {
        this.friendly = friendly;
    }

    public String toString() {
        return this.userID + " " + this.message + " " +  this.friendly.toString();
    }
}
