# Enzyme

Chat app based on the XMPP Protocol

## Installation and Usage

Check out www.enzymeapp.com

## Roadmap

Pre-Alpha Build 0.02 - mod_muc implementation

Alpha 1.0 - Functioning App

Beta 1.0 - Functioning App with most features working

Enzyme 0.01 - Inital Relase

## Project status

### Pre-Alpha Build 0.01

#### - Enzyme Client 

- Message functionality with GUI

#### - Enzyme Network
- Functioning backend for messaging over TLS and OMEMO

## Authors and acknowledgment
Ben Dattilio - <a rel="license" href="https://gitlab.com/coremed">@coremed</a>

Peter Waples - <a rel="license" href="https://gitlab.com/pawples">@pawples</a>

Augest Erbes - <a rel="license" href="https://gitlab.com/augusterbes">@augesterbes</a>

Will Im - <a rel="license" href="https://gitlab.com/willjcim">@willjcim</a>

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.

